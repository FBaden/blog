package de.academy.Blog.nutzer;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RegistrierungsDTO {

    //Felder
    @NotEmpty(message = "Darf nicht leer sein.")
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Darf nur Buchstaben und Zahlen enthalten.")
    private String nutzername;

    @Size(min = 5, message = "Muss mindestens 5 Zeichen lang sein.")
    private String passwort1;

    @Size(min = 5, message = "Muss mindestens 5 Zeichen lang sein.")
    private String passwort2;


    //Konstruktor
    public RegistrierungsDTO(String nutzername, String passwort1, String passwort2) {
        this.nutzername = nutzername;
        this.passwort1 = passwort1;
        this.passwort2 = passwort2;
    }


    //Getter
    public String getNutzername() {
        return nutzername;
    }

    public String getPasswort1() {
        return passwort1;
    }

    public String getPasswort2() {
        return passwort2;
    }
}