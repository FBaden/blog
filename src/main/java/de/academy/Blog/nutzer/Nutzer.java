package de.academy.Blog.nutzer;

import de.academy.Blog.beitrag.Aenderung;
import de.academy.Blog.beitrag.Beitrag;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Nutzer {

    //Felder
    @Id
    @GeneratedValue
    private int id;

    private String nutzername;

    private String passwort;

    @OneToMany(mappedBy = "nutzer")
    private List<Beitrag> beitraege;

    private String rolle; /*'admin' für Administrator, 'user' für registrierte Nutzer*/

    @OneToMany(mappedBy = "nutzer")
    private List<Aenderung> aenderungen;


    //Konstruktor
    public Nutzer() {
    }

    public Nutzer(String nutzername, String passwort, String rolle) {
        this.nutzername = nutzername;
        this.passwort = passwort;
        this.rolle = rolle;
    }


    //Getter
    public String getNutzername() {
        return nutzername;
    }

    public int getId() {
        return id;
    }

    public String getRolle() {
        return rolle;
    }


    //Setter
    public void setRolle(String rolle) {
        this.rolle = rolle;
    }
}