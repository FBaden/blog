package de.academy.Blog.nutzer;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NutzerRepository extends CrudRepository<Nutzer, Integer> {
    /**
     * Sucht einen Nutzer nach Nutzernamen und Passwort aus der Datenbank
     *
     * @param nutzername Der Nutzername nach dem gesucht wird
     * @param passwort   Das Passwort nach dem gesucht wird
     * @return Gibt einen Nutzer zurück, falls ein solcher existiert
     */
    Optional<Nutzer> findByNutzernameAndPasswort(String nutzername, String passwort);

    /**
     * Überprüft ob ein Nutzer existiert
     * @param nutzername Der Nutzername, anhand dessen die Überprüfung vollzogen wird
     * @return Gibt einen boolschen Wert ( true/false) zurück
     */
    boolean existsByNutzername(String nutzername);

    /**
     * Sucht alle Nutzer
     * @return Gibt eine Liste von allen Nutzern zurück
     */
    List<Nutzer> findAll();
}