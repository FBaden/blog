package de.academy.Blog.nutzer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
public class NutzerController {

    //Felder
    private NutzerRepository nutzerRepository;


    //Konstruktor
    @Autowired
    public NutzerController(NutzerRepository nutzerRepository) {
        this.nutzerRepository = nutzerRepository;
    }


    //Mapping

    /**
     * Behandelt den HTTP get request und zeigt ein leeres Formular zur Registrierung an
     *
     * @param model Das Model
     * @return Ruft das Template zur erstmaligen Registrierung eines neuen Nutzers auf
     */
    @GetMapping("/registrieren")
    public String register(Model model) {
        model.addAttribute("registrierung", new RegistrierungsDTO("", "", ""));
        return "RegistrierungsTemplate";
    }

    /**
     * Behandelt den HTTP post request und speichert die Nutzerdaten in der Datenbank
     * @param registrierung Übergibt die eingegebenen Daten an das Model und die Datenbank
     * @param bindingResult Überprüft die Validität der Eingaben
     * @return Führt einen Redirect auf das Login Template durch oder zeigt nochmals das Registrierungs Template bei
     * fehlerhaften Eingaben
     */
    @PostMapping("/registrieren")
    public String register(@Valid @ModelAttribute("registrierung") RegistrierungsDTO registrierung, BindingResult bindingResult) {
        if (!registrierung.getPasswort1().equals(registrierung.getPasswort2())) {
            bindingResult.addError(new FieldError("registrierung", "passwort2", "Passwörter stimmen nicht überein."));
        }
        if (nutzerRepository.existsByNutzername(registrierung.getNutzername())) {
            bindingResult.addError(new FieldError("registrierung", "nutzername", "Nutzername ist schon vergeben."));
        }
        if (bindingResult.hasErrors()) {
            return "RegistrierungsTemplate";
        }
        Nutzer nutzer = new Nutzer(registrierung.getNutzername(), registrierung.getPasswort1(), "user");
        nutzerRepository.save(nutzer);
        return "redirect:/login";
    }

    /**
     * Behandelt den HTTP get request und zeigt die Liste aller Nutzer an
     * @param model Das Model
     * @param sessionNutzer Der aktuell eingeloggte Nutzer
     * @return Führt einen Redirect auf den Homescreen bei fehlenden Rechten duch oder ruft das Template der
     * Nutzerliste auf
     */
    @GetMapping("/nutzer")
    public String nutzerliste(Model model, @ModelAttribute("sessionNutzer") Nutzer sessionNutzer) {
        if (sessionNutzer.getRolle().equals("admin")) {
            List<Nutzer> nutzerList = nutzerRepository.findAll();
            model.addAttribute("nutzerList", nutzerList);
            return "NutzerlisteTemplate";
        } else {
            return "redirect:/home";
        }
    }

    /**
     * Behandelt den HTTP post request und ändert die Rolle eines Nutzers in der Datenbank zu Administrator
     * @param nutzerId Die Id des Nutzers zur eindeutigen Identifizierung
     * @return Führt einen Redirect auf das Template der Nutzerliste durch
     */
    @PostMapping("/nutzer/admin")
    public String admin(@RequestParam int nutzerId) {
        Nutzer nutzer = nutzerRepository.findById(nutzerId).orElseThrow();
        nutzer.setRolle("admin");
        nutzerRepository.save(nutzer);
        return "redirect:/nutzer";
    }

    /**
     * Behandelt den HTTP post request und ändert die Rolle eines Nutzers in der Datenbank zu Moderator
     *
     * @param nutzerId Die Id des Nutzers zur eindeutigen Identifizierung
     * @return Führt einen Redirect auf das Template der Nutzerliste durch
     */
    @PostMapping("/nutzer/moderate")
    public String mod(@RequestParam int nutzerId) {
        Nutzer nutzer = nutzerRepository.findById(nutzerId).orElseThrow();
        nutzer.setRolle("mod");
        nutzerRepository.save(nutzer);
        return "redirect:/nutzer";
    }
}