package de.academy.Blog.home;

import de.academy.Blog.beitrag.BeitragRepository;
import de.academy.Blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class HomeController {

    //Felder
    private BeitragRepository beitragRepository;


    //Konstruktor
    @Autowired
    public HomeController(BeitragRepository beitragRepository) {
        this.beitragRepository = beitragRepository;
    }


    //Mapping

    /**
     * Behandelt den HTTP get request und zeigt den Home Screen an
     *
     * @param model         Das Model
     * @param sessionNutzer Der aktuell eingelogte Nutzer
     * @return Ruft das Template für den Homescreen auf
     */
    @GetMapping("/home")
    public String home(Model model, @ModelAttribute("sessionNutzer") Nutzer sessionNutzer) {
        model.addAttribute("beitraege", beitragRepository.findAllByOrderByDatumDesc());
        return "homeTemplate";
    }
}