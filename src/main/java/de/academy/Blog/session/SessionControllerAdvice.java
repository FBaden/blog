package de.academy.Blog.session;

import de.academy.Blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.time.Instant;
import java.util.Optional;

@ControllerAdvice
public class SessionControllerAdvice {

    //Felder
    private SessionRepository sessionRepository;


    //Konstruktor
    @Autowired
    public SessionControllerAdvice(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }


    //Attribut

    /**
     * Erstellt ein Model Attribute das an alle Mappings übergeben werden kann und überprüft ob es eine aktive Session
     * gibt
     *
     * @param sessionId Die Id der Session zur Identifizierung eines Nutzers
     * @return Gibt den Session Nutzer zurück oder den Wert null bei keiner aktiven Session
     */
    @ModelAttribute("sessionNutzer")
    public Nutzer sessionNutzer(@CookieValue(value = "sessionId", defaultValue = "") String sessionId) {
        if (!sessionId.isEmpty()) {
            Optional<Session> optionalSession = sessionRepository.findByIdAndExpiresAtAfter(
                    sessionId, Instant.now());
            if (optionalSession.isPresent()) {
                Session session = optionalSession.get();
                // neues Ablaufdatum für die Session
                session.setExpiresAt(Instant.now().plusSeconds(7 * 24 * 60 * 60));
                return session.getNutzer();
            }
        }
        return null;
    }
}