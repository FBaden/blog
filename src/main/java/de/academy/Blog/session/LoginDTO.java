package de.academy.Blog.session;

public class LoginDTO {

    //Felder
    private String nutzername;
    private String passwort;


    // Konstruktor
    public LoginDTO(String nutzername, String passwort) {
        this.nutzername = nutzername;
        this.passwort = passwort;
    }


    // Getter
    public String getNutzername() {
        return nutzername;
    }

    public String getPasswort() {
        return passwort;
    }
}