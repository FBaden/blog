package de.academy.Blog.session;

import de.academy.Blog.nutzer.Nutzer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;
import java.util.UUID;

@Entity
public class Session {

    //Felder
    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    private Nutzer nutzer;

    private Instant expiresAt;


    // Konstruktor
    public Session() {
    }

    public Session(Nutzer nutzer, Instant expiresAt) {
        this.nutzer = nutzer;
        this.expiresAt = expiresAt;
    }


    // Getter
    public String getId() {
        return id;
    }

    public Nutzer getNutzer() {
        return nutzer;
    }


    // Setter
    public void setExpiresAt(Instant expiresAt) {
        this.expiresAt = expiresAt;
    }
}