package de.academy.Blog.session;

import de.academy.Blog.nutzer.Nutzer;
import de.academy.Blog.nutzer.NutzerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.Optional;

@Controller
public class SessionController {

    //Felder
    private NutzerRepository nutzerRepository;

    private SessionRepository sessionRepository;


    //Konstruktor
    @Autowired
    public SessionController(NutzerRepository nutzerRepository, SessionRepository sessionRepository) {
        this.nutzerRepository = nutzerRepository;
        this.sessionRepository = sessionRepository;
    }


    //Mapping

    /**
     * Behandelt den HTTP get request und zeigt das Template zum Login
     *
     * @param model Das Model
     * @return Ruft das Login Template auf
     */
    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("login", new LoginDTO("", ""));
        return "LoginTemplate";
    }

    /**
     * Behandelt den HTTP post request und speichert die Session des Nutzers
     * @param login Überträgt die eingegebenen Daten aus dem Login Formular
     * @param bindingResult Überprüft die Validität der Eingaben
     * @param response Darin wird der Cookie gespeichert
     * @return Führt einen Redirect auf den Homescreen durch oder ruft das Login Template bei fehlerhaften Eingaben auf
     */
    @PostMapping("/login")
    public String login(@ModelAttribute("login") LoginDTO login, BindingResult bindingResult, HttpServletResponse response) {
        Optional<Nutzer> optionalNutzer = nutzerRepository.findByNutzernameAndPasswort(login.getNutzername(), login.getPasswort());
        if (optionalNutzer.isPresent()) {
            Session session = new Session(optionalNutzer.get(), Instant.now().plusSeconds(7 * 24 * 60 * 60));
            sessionRepository.save(session);
            Cookie cookie = new Cookie("sessionId", session.getId());
            response.addCookie(cookie);
            // Login erfolgreich
            return "redirect:/home";
        }
        bindingResult.addError(new FieldError("login", "passwort", "Login fehlgeschlagen."));
        return "LoginTemplate";
    }

    /**
     * Behandelt den HTTP post request, löscht den Cookie und beendet die aktuelle Session
     * @param sessionId Die Id der Session zur eindeutigen Identifizierung der Session
     * @param response Daraus wird der Cookie gelöscht
     * @return Führt einen Redirect auf den Homescreen durch
     */
    @PostMapping("/logout")
    public String logout(@CookieValue(value = "sessionId", defaultValue = "") String sessionId, HttpServletResponse response) {
        Optional<Session> optionalSession = sessionRepository.findByIdAndExpiresAtAfter(sessionId, Instant.now());
        optionalSession.ifPresent(session -> sessionRepository.delete(session));
        Cookie cookie = new Cookie("sessionId", "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
        return "redirect:/home";
    }
}