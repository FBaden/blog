package de.academy.Blog.session;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Optional;

@Repository
public interface SessionRepository extends CrudRepository<Session, String> {
    /**
     * Sucht eine Session nach ihrer Id und ihrem Ablaufdatum
     *
     * @param id        Die Id der Session nach der gesucht wird
     * @param expiresAt Das Ablaufdatum nach dem gesucht wird
     * @return Gibt eine Session zurück, falls eine solche existiert
     */
    Optional<Session> findByIdAndExpiresAtAfter(String id, Instant expiresAt);
}