package de.academy.Blog.beitrag;

import de.academy.Blog.kommentar.Kommentar;
import de.academy.Blog.kommentar.KommentarRepository;
import de.academy.Blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;

@Controller
public class BeitragController {

    //Felder
    private BeitragRepository beitragRepository;
    private KommentarRepository kommentarRepository;
    private AenderungRepository aenderungRepository;


    //Konstruktor
    @Autowired
    public BeitragController(BeitragRepository beitragRepository, KommentarRepository kommentarRepository, AenderungRepository aenderungRepository)
    {
        this.beitragRepository = beitragRepository;
        this.kommentarRepository = kommentarRepository;
        this.aenderungRepository = aenderungRepository;
    }


    //Mappings
    /**
     * Erzeugt das Template für einen neuen Beitrag mit leerem Titel und Textfeld
     *
     * @param model Das Model
     * @return Ruft das Template zur Erstellung eines Beitrages auf
     */
    @GetMapping("/beitrag")
    public String beitrag(Model model) {
        model.addAttribute("beitrag", new BeitragDTO("", ""));
        return "BeitragTemplate";
    }

    /**
     * Behandelt den HTTP post request und speichert den neuen Beitrag in der Datenbank
     * @param beitragDTO Übergibt die eingegebenen Daten aus dem Beitragsformular
     * @param sessionNutzer Der Nutzer der momentan eingelogt ist
     * @return Führt einen Redirect auf den Home Screen durch
     */
    @PostMapping("/beitrag")
    public String beitrag(@Valid @ModelAttribute("beitrag") BeitragDTO beitragDTO, BindingResult bindingResult,
                          @ModelAttribute("sessionNutzer") Nutzer sessionNutzer) {
        if (bindingResult.hasErrors()) {
            return "BeitragTemplate";
        }
        Beitrag beitrag = new Beitrag(beitragDTO.getTitel(), beitragDTO.getText(), Instant.now(), sessionNutzer);
        beitragRepository.save(beitrag);
        return "redirect:/home";
    }

    /**
     * Behandelt den HTTP post request und löscht den Beitrag aus der Datenbank
     * @param beitragId Die Id des Beitrags zur eindeutigen Identifizierung
     * @param sessionNutzer Der Nutzer der momentan eingelogt ist
     * @return Führt einen Redirect auf den Home Screen durch
     */
    @PostMapping("/beitragDelete")
    public String delete(@RequestParam int beitragId, @ModelAttribute("sessionNutzer") Nutzer sessionNutzer) {
        if (sessionNutzer.getRolle().equals("admin")) {
            Beitrag beitrag = beitragRepository.findById(beitragId).orElseThrow();
            List<Kommentar> kommentare = beitrag.getKommentare();
            kommentarRepository.deleteAll(kommentare);
            beitragRepository.delete(beitrag);
        }
        return "redirect:/home";
    }

    /**
     * Behandelt den HTTP get request und zeigt das Formular zur Ausbesserung eines Beitrages an
     * @param model Das Model
     * @param id Die Id des Beitrags für den URL Pfad und die eindeutige Identifizierung des Beitrags
     * @return Ruft das Ausbesserungstemplate des Beitrags auf
     */
    @GetMapping("/{id}/update")
    public String update(Model model, @PathVariable int id) {
        Beitrag beitrag = beitragRepository.findById(id).orElseThrow();
        BeitragDTO beitragDTO = new BeitragDTO(beitrag.getTitel(), beitrag.getText());
        beitragDTO.setId(id);
        model.addAttribute("beitragDTO", beitragDTO);
        return "UpdateTemplate";
    }

    /**
     * Behandelt den HTTP post request und überschreibt den Beitrag in der Datenbank und speichert eine Änderung
     * @param beitragDTO Übergibt die eingegebenen Daten aus dem Beitragsformular
     * @param sessionNutzer Der aktuelle Nutzer, der eingelogt ist
     * @param id Die Id für die URL und die eindeutige Identifizierung des Beitrags
     * @return Führt einen Redirect auf die Detailansicht des Beitrags durch
     */
    @PostMapping("/{id}/update")
    public String update(@Valid @ModelAttribute("beitragDTO") BeitragDTO beitragDTO, BindingResult bindingResult, @ModelAttribute("sessionNutzer")
            Nutzer sessionNutzer, @PathVariable int id) {
        if (bindingResult.hasErrors()) {
            return "UpdateTemplate";
        }
        if (sessionNutzer.getRolle().equals("admin")) {
            Beitrag beitrag = beitragRepository.findById(id).orElseThrow();
            Aenderung aenderung = new Aenderung(beitrag, sessionNutzer, Instant.now());
            beitrag.setTitel(beitragDTO.getTitel());
            beitrag.setText(beitragDTO.getText());
            beitragRepository.save(beitrag);
            aenderungRepository.save(aenderung);
        }
        return "redirect:/{id}/kommentar";
    }
}