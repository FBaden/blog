package de.academy.Blog.beitrag;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BeitragRepository extends CrudRepository<Beitrag, Integer> {
    /**
     * Sucht alle Beiträge in der umgekehrten Reihenfolge des Datums, also der Neueste zuerst
     *
     * @return Gibt eine Liste an Beiträgen zurück
     */
    List<Beitrag> findAllByOrderByDatumDesc();

    /**
     * Sucht Beiträge nach ihrer Id
     * @param id Die Id nach der gesucht wird
     * @return Gibt einen Beitrag zurück, falls die Beitrags-Id existiert
     */
    Optional<Beitrag> findById(int id);
}