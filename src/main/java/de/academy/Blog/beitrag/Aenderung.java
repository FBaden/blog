package de.academy.Blog.beitrag;

import de.academy.Blog.nutzer.Nutzer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class Aenderung
{
    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    private Beitrag beitrag;

    @ManyToOne
    private Nutzer nutzer;

    private Instant zeit;

    //Konstruktor
    public Aenderung()
    {
    }

    public Aenderung(Beitrag beitrag, Nutzer nutzer, Instant zeit)
    {
        this.beitrag = beitrag;
        this.nutzer = nutzer;
        this.zeit = zeit;
    }

    // Getter
    public int getId()
    {
        return id;
    }

    public Beitrag getBeitrag()
    {
        return beitrag;
    }

    public Nutzer getNutzer()
    {
        return nutzer;
    }

    public Instant getZeit()
    {
        return zeit;
    }
}
