package de.academy.Blog.beitrag;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AenderungRepository extends CrudRepository<Aenderung, Integer>
{
    /**
     * Sucht Änderungen nach je Beitrag
     *
     * @param beitragId ist die ID, nach der gescuht wird
     * @return eine Liste an allen Änderungen für diesen Beitrag
     */
    List<Aenderung> findByBeitragId(int beitragId);
}
