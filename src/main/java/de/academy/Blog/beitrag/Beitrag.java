package de.academy.Blog.beitrag;

import de.academy.Blog.kommentar.Kommentar;
import de.academy.Blog.nutzer.Nutzer;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
public class Beitrag {

    //Felder
    @Id
    @GeneratedValue
    private int id;

    private String titel;

    private String text;

    private Instant datum;

    @ManyToOne
    private Nutzer nutzer;

    @OneToMany(mappedBy = "beitrag")
    private List<Kommentar> kommentare;

    @OneToMany(mappedBy = "beitrag")
    private List<Aenderung> aenderungen;


    // Konstruktor
    public Beitrag() {
    }

    public Beitrag(String titel, String text, Instant datum, Nutzer nutzer) {
        this.titel = titel;
        this.text = text;
        this.datum = datum;
        this.nutzer = nutzer;
    }


    // Getter
    public int getId() {
        return id;
    }

    public String getTitel() {
        return titel;
    }

    public String getText() {
        return text;
    }

    public Instant getDatum() {
        return datum;
    }

    public Nutzer getNutzer() {
        return nutzer;
    }

    public List<Kommentar> getKommentare() {
        return kommentare;
    }


    //Setter
    public void setTitel(String titel) {
        this.titel = titel;
    }

    public void setText(String text) {
        this.text = text;
    }
}