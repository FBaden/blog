package de.academy.Blog.beitrag;

import javax.validation.constraints.Size;

public class BeitragDTO {

    //Felder
    private int id;

    @Size(min = 1, max = 99, message = "Muss zwischen 1 und maximal 99 Zeichen lang sein.")
    private String titel;

    private String text;


    // Konstruktor
    public BeitragDTO(String titel, String text) {
        this.titel = titel;
        this.text = text;
    }


    // Getter
    public String getTitel() {
        return titel;
    }

    public String getText() {
        return text;
    }

    public int getId() {
        return id;
    }


    //Setter
    public void setId(int id) {
        this.id = id;
    }
}