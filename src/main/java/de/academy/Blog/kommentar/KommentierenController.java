package de.academy.Blog.kommentar;

import de.academy.Blog.beitrag.AenderungRepository;
import de.academy.Blog.beitrag.Beitrag;
import de.academy.Blog.beitrag.BeitragRepository;
import de.academy.Blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;

@Controller
public class KommentierenController {

    //Felder
    private KommentarRepository kommentarRepository;
    private BeitragRepository beitragRepository;
    private AenderungRepository aenderungRepository;


    //Konstruktor
    @Autowired
    public KommentierenController(KommentarRepository kommentarRepository, BeitragRepository beitragRepository, AenderungRepository aenderungRepository)
    {
        this.kommentarRepository = kommentarRepository;
        this.beitragRepository = beitragRepository;
        this.aenderungRepository = aenderungRepository;
    }


    //Mapping
    /**
     * Behandelt den HTTP get request und zeigt das Formular zur Kommentierung eines Beitrages an und übergibt die Liste an Änderungen (falls vorhanden)
     * @param model Das Model
     * @param id    Die Id des Beitrags für die URL und zur eindeutigen Identifizierung des Beitrags
     * @return Ruft das Template für die Detailansicht eines Beitrags mit Kommentarfunktion auf
     */
    @GetMapping("/{id}/kommentar")
    public String kommentar(Model model, @PathVariable int id) {
        Beitrag beitrag = beitragRepository.findById(id).orElseThrow();
        model.addAttribute("kommentar", new KommentarDTO(""));
        model.addAttribute("beitrag", beitrag);
        model.addAttribute("kommentare", kommentarRepository.findByBeitragOrderByDatumAsc(beitrag));
        model.addAttribute("aenderungen", aenderungRepository.findByBeitragId(beitrag.getId()));
        return "KommentierenTemplate";
    }

    /**
     * Behandelt den HTTP post request und speichert den Kommentar zum Beitrag in der Datenbank
     * @param kommentarDTO  Übergibt die eingegebenen Daten aus dem Kommentarformular
     * @param sessionNutzer Der aktuell eingelogt Nutzer
     * @param id Die Id des Beitrags zur eindeutigen Identifizierung und für den URL-Pfad
     * @return Führt einen Redirect auf die Detailansicht des Beitrags durch
     */
    @PostMapping("/{id}/kommentar")
    public String kommentar(@Valid @ModelAttribute("kommentar") KommentarDTO kommentarDTO, BindingResult bindingResult, @ModelAttribute("sessionNutzer")
            Nutzer sessionNutzer, @PathVariable int id) {
        if (bindingResult.hasErrors()) {
            return "redirect:/{id}/kommentar";
        }
        Beitrag beitrag = beitragRepository.findById(id).orElseThrow();
        Kommentar kommentar = new Kommentar(kommentarDTO.getText(), Instant.now(), sessionNutzer, beitrag);
        kommentarRepository.save(kommentar);
        return "redirect:/{id}/kommentar";
    }

    /**
     * Behandelt den HTTP post request und löscht den Kommentar aus der Datenbank
     * @param kommentarId Die Id des Kommentars zur eindeutigen Identifizierung
     * @param sessionNutzer Der aktuell eingelogte Nutzer
     * @return Führt einen Redirect auf die Detailansicht des Beitrags durch
     */
    @PostMapping("/{id}/commentDelete")
    public String delete(@RequestParam int kommentarId, @ModelAttribute("sessionNutzer") Nutzer sessionNutzer) {
        Kommentar kommentar = kommentarRepository.findById(kommentarId).orElseThrow();
        if (sessionNutzer.getRolle().equals("admin") || sessionNutzer == kommentar.getNutzer() ||
                sessionNutzer.getRolle().equals("mod")) {
            kommentarRepository.delete(kommentar);
        }
        return "redirect:/{id}/kommentar";
    }
}