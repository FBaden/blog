package de.academy.Blog.kommentar;

import de.academy.Blog.beitrag.Beitrag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KommentarRepository extends CrudRepository<Kommentar, Integer> {
    /**
     * Sucht alle Kommentare zu einem Beitrag und sortiert diese nach dem Datum, der älteste Kommentar zuerst
     *
     * @param beitrag Der Beitrag, dessen Kommentare gesucht werden sollen
     * @return Gibt eine sortierte Liste an Kommentaren zurück
     */
    List<Kommentar> findByBeitragOrderByDatumAsc(Beitrag beitrag);
}