package de.academy.Blog.kommentar;

import de.academy.Blog.beitrag.Beitrag;
import de.academy.Blog.nutzer.Nutzer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class Kommentar {

    //Felder
    @Id
    @GeneratedValue
    private int id;

    private String text;

    private Instant datum;

    @ManyToOne
    private Nutzer nutzer;

    @ManyToOne
    private Beitrag beitrag;


    // Konstruktor
    public Kommentar() {
    }

    public Kommentar(String text, Instant datum, Nutzer nutzer, Beitrag beitrag) {
        this.text = text;
        this.datum = datum;
        this.nutzer = nutzer;
        this.beitrag = beitrag;
    }


    // Getter
    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Nutzer getNutzer() {
        return nutzer;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }
}