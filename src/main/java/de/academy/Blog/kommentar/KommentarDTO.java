package de.academy.Blog.kommentar;

import javax.validation.constraints.Size;

public class KommentarDTO {

    //Felder
    @Size(min = 1, max = 199, message = "Muss zwischen 1 und maximal 199 Zeichen lang sein.")
    private String text;


    // Konstruktor
    public KommentarDTO(String text) {
        this.text = text;
    }


    // Getter
    public String getText() {
        return text;
    }
}